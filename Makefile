
lint:
  go get -u gopkg.in/alecthomas/gometalinter.v2
  gometalinter.v2 --install
	gometalinter.v2 ./... | grep -v -E "(should have comment or be unexported|comment on exported method)" || true
	gometalinter.v2 ./... --errors
.PHONY: lint

test:
	go test -v ./...
.PHONY: test

install:
  go install ./cmd/json2ssm
.PHONY: install
