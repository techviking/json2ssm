//+build mage

package main

import (
	"os"
	"os/exec"
	"fmt"
)

type commandList []*exec.Cmd

func (cl commandList) run() error {
	var cmdErr error

	for _, curCmd := range cl {
		curCmd.Stderr = os.Stderr
		curCmd.Stdout = os.Stdout

		if cmdErr = curCmd.Run(); cmdErr != nil {
			fmt.Printf("Error running command: %s",  cmdErr)
			return cmdErr
		}
	}

	return nil
}

// Lint run go metalinter.
func Lint() error {
	return commandList{
		exec.Command("go", "get", "\"https://gopkg.in/alecthomas/gometalinter.v2\""),
		exec.Command("gometalinter.v2", "--install"),
		exec.Command("gometalinter.v2", "./...", "|", "grep", "-v", "-E", "\"(should have comment or be unexported|comment on exported method)\"", "||", "true"),
		exec.Command("gometalinter.v2", "./...", "--errors"),
	}.run()
}

func Test() error {
	return commandList{
		exec.Command("go", "test",  "-v", "./..."),
	}.run()
}

func Install() error {
	return commandList{
		exec.Command("go", "install", "./cmd/json2ssm"),
	}.run()
}
